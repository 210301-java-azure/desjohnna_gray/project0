package test_services;

import dev.gray.building_log_models.Employee;
import dev.gray.repos.UserRepository;
import dev.gray.services.LoginServiceImpl;
import dev.gray.token.AuthRequest;
import dev.gray.token.AuthResponse;
import dev.gray.token.TokenManager;
import org.junit.BeforeClass;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class LoginServiceImplTest {


    @InjectMocks

    private LoginServiceImpl loginService;

    @Mock
    private UserRepository repository;
    private TokenManager manager;

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }


    @BeforeClass
    public void setup() {
        repository = mock(UserRepository.class);
        manager = mock(TokenManager.class);
        loginService = new LoginServiceImpl(repository, manager);
    }

    @Test
    public void loginTest() {
        final AuthRequest request = new AuthRequest("user@email.com", "secret");
        final String token = "token";
        final Employee user = new Employee(1, "user@email.com", "secret");
        when(repository.findByEmail("user@email.com")).thenReturn(Optional.of(user));

    }

    @Test
    public void signupTest() {
        final AuthRequest request = new AuthRequest("user@email.com", "secret");
        final String token = "token";
        final Employee user = new Employee(1, "user@email.com", "secret");
        when(repository.signup("user@email.com", "secret")).thenReturn(user);
        AuthResponse result = loginService.signup(request);
        assert (result.getToken().equals(token));
        assert (result.getToken()).matches(token);
    }

}
